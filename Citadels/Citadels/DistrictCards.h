#pragma once
#include<iostream>
#include<vector>
#include<unordered_map>
#include<fstream>

class DistrictCards
{
public:
	enum class Type
	{
		Trade,
		Noble,
		Military,
		Religious,
		Unique,
		None
	};
	enum class Color
	{
		Green,
		Yellow,
		Red,
		Blue,
		Purple,
		None
	};
	enum class Name
	{
		Tavern,
		Market,
		TradingPost,
		TownHall,
		Harbor,
		Docks,
		Castel,
		Manor,
		Palace,
		Watchtower,
		Barracks,
		Prison,
		Fortress,
		Cathedral,
		Church,
		Temple,
		Monastery,
		ImperialTreasury,
		DragonGate,
		Graveyard,
		MapRoom,
		SchoolOfMagic,
		Keep,
		GreatWall,
		Laboratory,
		Observatory,
		HauntedQuarter,
		University,
		Library,
		Smithy,
		None
	};
public:
	DistrictCards();
	DistrictCards(Type type,Color color,int value);
	DistrictCards(DistrictCards &&other);
	DistrictCards &operator =(DistrictCards &&other);
	virtual ~DistrictCards();
	
	friend std::ostream& operator<<(std::ostream& out, const DistrictCards* district);
	const void setType(Type type);
	const Type getType() const;
	const void setColor(Color color);
	const Color getColor() const;
	const Name getName() const;
	const void setName(Name name);
	const void setValue(int value);
	const int getValue() const;
	const void setnCards(int nCards);
	const int getnCards() const;
	const void setAbility(std::string& ability);
	const std::string getAbility() const;

	DistrictCards* pickCard();
	void createDeck();
	void printAllDistrictCards();
private:
	Type m_type;
	Color m_color;
	Name m_name;
	int m_value;
	int m_nCards;
	std::string m_ability;
	std::unordered_map<DistrictCards*, int> m_shuffledDistrictCards;
};

