#pragma once
#include"CharacterCards.h"
#include"DistrictCards.h"
#include"Player.h"
#include"BoardStateChecker.h"

class CitadelsGame
{
public:
	int k_nrOfRounds = 0;
public:
	void Init();
	void playerTurn(Player &firstplayer,Player &secondplayer);
	void buildDistricts(Player &player);
	std::vector<int> aiAssistant(Player &firstplayer, Player &secondplayer);
	void aiGetBestMove(std::vector<int> vector);
private:
	DistrictCards m_deck;
	CharacterCards m_characters;
	Player firstPlayer;
	Player secondPlayer;
};

