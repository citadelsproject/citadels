#include "Player.h"

Player::Player() :
	m_name("unknown"),
	m_gold(2)
{
}

Player::Player(const std::string & name) :
	m_name(name),
	m_gold(2),
	m_king(false)
{
}

Player::~Player()
{
}

const void Player::addGold(int gold)
{
	m_gold += gold;
}

const int Player::getGold() const
{
	return m_gold;
}

const std::string Player::getName() const
{
	return m_name;
}

const void Player::setName(std::string &name)
{
	m_name = name;
}

const void Player::setGold(int gold)
{
	m_gold = gold;
}

const Hand Player::getHand() const
{
	return m_handCards;
}

const void Player::setHand(Hand  hand)
{
	 m_handCards = hand;
}

const void Player::setKing(bool king)
{
	m_king = true;
}

const bool Player::getKing() const
{
	return m_king;
}

int Player::getValueOfCard(int position)
{
	return m_handCards.getCard(position);
}

void Player::addDistrictCard(DistrictCards * &card)
{
	m_handCards.addHandCards(card);
}

DistrictCards* Player::dropDistrictCard(int position)
{
	return m_handCards.dropHandCards(position);
}

void Player::displayAllCards()
{
	m_handCards.displayHandCards();
}

bool Player::canBuild(int gold)
{
	return m_handCards.canBuild(gold);
}

int Player::nrOfHandCards()
{
	return m_handCards.nrOfCards();
}

int Player::nrOfRedDistricts()
{
	int k = 0;
	for (auto it = m_boardPlayer.m_cards.begin(); it < m_boardPlayer.m_cards.end(); ++it)
	{
		if (*it != std::nullopt)
		{
			if (it->value()->getColor() == DistrictCards::Color::Red)
				k++;
		}
	}
	return k;
}

int Player::nrOfBlueDistricts()
{
	int k = 0;
	for (auto it = m_boardPlayer.m_cards.begin(); it < m_boardPlayer.m_cards.end(); ++it)
	{
		if (*it != std::nullopt)
		{
			if (it->value()->getColor() == DistrictCards::Color::Blue)
				k++;
		}
	}
	return k;
}

int Player::nrOfYellowDistricts()
{
	int k = 0;
	for (auto it = m_boardPlayer.m_cards.begin(); it < m_boardPlayer.m_cards.end(); ++it)
	{
		if (*it != std::nullopt)
		{
			if (it->value()->getColor() == DistrictCards::Color::Yellow)
				k++;
		}
	}
	return k;
}

int Player::nrOfGreenDistricts()
{
	int k = 0;
	for (auto it = m_boardPlayer.m_cards.begin(); it < m_boardPlayer.m_cards.end(); ++it)
	{
		if (*it != std::nullopt)
		{
			if (it->value()->getColor() == DistrictCards::Color::Green)
				k++;
		}
	}
	return k;
}


