#pragma once
#include "DistrictCards.h"

#include <array>
#include <optional>

class Board
{
public:
	static const size_t nrOfDistrictCards = 6;
public:
	Board() = default;
	~Board() = default;
	const void DrawBoard();
	void PlaceDistrict(DistrictCards* &dc,int position);
	DistrictCards* DestroyDistrict(int position);
	int nBoardCards();
	int getAllGold();
	
	std::optional<DistrictCards*>& operator[](const int &position);
	std::array<std::optional<DistrictCards*>,nrOfDistrictCards> m_cards;
};

