#include "BoardStateChecker.h"


BoardStateChecker::State BoardStateChecker::Check(Player & firstplayer, Player & secondplayer)
{
	Board board1 = firstplayer.m_boardPlayer;
	Board board2 = secondplayer.m_boardPlayer;
	if (board1.getAllGold() > board2.getAllGold())
	{
		return State::Win;
	}
	else if (board1.getAllGold() == board2.getAllGold())
	{
		return State::Draw;
	}
	else
		return State::None;
}
