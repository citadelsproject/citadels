#include "DistrictCards.h"

#include<fstream>
#include<string>

DistrictCards::DistrictCards():
	m_type(Type::None),
	m_color(Color::None),
	m_value(0)
{
}

DistrictCards::DistrictCards(DistrictCards::Type type, DistrictCards::Color color, int value):
m_type(type),
m_color(color),
m_value(value)
{
}

DistrictCards::DistrictCards(DistrictCards &&other)
{
	*this = std::move(other);
}

DistrictCards & DistrictCards::operator=(DistrictCards && other)
{
	new(&other) DistrictCards;
	return *this;
}

DistrictCards::~DistrictCards()
{
}

const void DistrictCards::setType(Type type)
{
	m_type = type;
}

const DistrictCards::Type DistrictCards::getType() const
{
	return m_type;
}

const void DistrictCards::setColor(Color color)
{
	m_color = color;
}

const DistrictCards::Color DistrictCards::getColor() const
{
	return m_color;
}

const DistrictCards::Name DistrictCards::getName() const
{
	return m_name;
}

const void DistrictCards::setName(Name name)
{
	m_name = name;
}

const void DistrictCards::setValue(int value)
{
	m_value = value;
}

const int DistrictCards::getValue() const
{
	return m_value;

}
const void DistrictCards::setnCards(int nCards)
{
	m_nCards = nCards;
}
const int DistrictCards::getnCards() const
{
	return m_nCards;
}
const void DistrictCards::setAbility(std::string& ability)
{
	m_ability = ability;
}
const std::string DistrictCards::getAbility() const
{
	return m_ability;
}
DistrictCards* DistrictCards::pickCard()
{
	if (m_shuffledDistrictCards.size() - 1 == 0)
		throw "Deck is empty";

	int random = rand() % m_shuffledDistrictCards.size();
	auto it = m_shuffledDistrictCards.begin();
	while (random)
	{
		it++;
		random--;
	}
	DistrictCards *card = it->first;
	if (it->second > 1)
		it->second--;
	else
		if (it->second == 1)
			m_shuffledDistrictCards.extract(std::move(it));

	return card;
}
void DistrictCards::createDeck()
{
	int index = 0;
	int value;
	std::string ability;
	std::ifstream fin("Values.txt");
	std::ifstream finPurple("addAllPurpleCards.txt");

	while (index < 6)
	{
		DistrictCards *card = new DistrictCards();
		card->setType(DistrictCards::Type::Trade);
		card->setColor(DistrictCards::Color::Green);
		card->setName(static_cast<DistrictCards::Name>(index));
		fin >> value;
		card->setValue(value);
		fin >> value;
		card->setnCards(value);
		m_shuffledDistrictCards.insert(std::make_pair(card, value));
		index++;
	}
	while (index < 9)
	{
		DistrictCards *card = new DistrictCards();
		card->setType(DistrictCards::Type::Noble);
		card->setColor(DistrictCards::Color::Yellow);
		card->setName(static_cast<DistrictCards::Name>(index));
		fin >> value;
		card->setValue(value);
		fin >> value;
		card->setnCards(value);
		m_shuffledDistrictCards.insert(std::make_pair(card, value));
		index++;
	}
	while (index < 13)
	{
		DistrictCards *card = new DistrictCards();
		card->setType(DistrictCards::Type::Military);
		card->setColor(DistrictCards::Color::Red);
		card->setName(static_cast<DistrictCards::Name>(index));
		fin >> value;
		card->setValue(value);
		fin >> value;
		card->setnCards(value);
		m_shuffledDistrictCards.insert(std::make_pair(card, value));
		index++;
	}
	while (index < 17)
	{
		DistrictCards *card = new DistrictCards();
		card->setType(DistrictCards::Type::Religious);
		card->setColor(DistrictCards::Color::Blue);
		card->setName(static_cast<DistrictCards::Name>(index));
		fin >> value;
		card->setValue(value);
		fin >> value;
		card->setnCards(value);
		m_shuffledDistrictCards.insert(std::make_pair(card, value));
		index++;
	}
	while (index < 30)
	{
		DistrictCards *card = new DistrictCards();
		card->setType(DistrictCards::Type::Unique);
		card->setColor(DistrictCards::Color::Purple);
		card->setName(static_cast<DistrictCards::Name>(index));
		std::getline(fin, ability);
		card->setAbility(ability);
		fin >> value;
		card->setValue(value);
		fin >> value;
		card->setnCards(value);
		m_shuffledDistrictCards.insert(std::make_pair(card, value));
		index++;
	}
}
void DistrictCards::printAllDistrictCards()
{
	for (auto it = m_shuffledDistrictCards.begin(); it != m_shuffledDistrictCards.end();++it)
		std::cout << it->first << " " << it->second << std::endl;
}
std::ostream & operator<<(std::ostream & out, const DistrictCards* district)
{
	switch (static_cast<int>(district->getColor()))
	{
	case 0:out << "Green "; break;
	case 1:out << "Yellow "; break;
	case 2:out << "Red "; break;
	case 3:out << "Blue "; break;
	case 4:out << "Purple "; break;
	case 5:out << "None "; break;
	default:
		break;
	}

	switch (static_cast<int>(district->getType()))
	{
	case 0:out<<"Trade "; break;
	case 1:out << "Noble "; break;
	case 2:out << "Military "; break;
	case 3:out << "Religious "; break;
	case 4:out << "Unique "; break;
	case 5:out << "None "; break;
	default:
		break;
	}
	switch (static_cast<int>(district->getName()))
	{
	case 0:out << "Tavern "; break;
	case 1:out << "Market "; break;
	case 2:out << "TradingPost "; break;
	case 3:out << "TownHall "; break;
	case 4:out << "Harbor "; break;
	case 5:out << "Docks "; break;
	case 6:out << "Castel "; break;
	case 7:out << "Manor "; break;
	case 8:out << "Palace "; break;
	case 9:out << "Watchtower "; break;
	case 10:out << "Barracks "; break;
	case 11:out << "Prison "; break;
	case 12:out << "Fortress "; break;
	case 13:out << "Cathedral "; break;
	case 14:out << "Church "; break;
	case 15:out << "Temple "; break;
	case 16:out << "Monastery "; break;
	case 17:out << "ImperialTreasury "; break;
	case 18:out << "DragonGate "; break;
	case 19:out << "Graveyard "; break;
	case 20:out << "MapRoom "; break;
	case 21:out << "SchoolOfMagic "; break;
	case 22:out << "Keep "; break;
	case 23:out << "GreatWall "; break;
	case 24:out << "Laboratory "; break;
	case 25:out << "Observatory "; break;
	case 26:out << "HauntedQuarter "; break;
	case 27:out << "University "; break;
	case 28:out << "Library "; break;
	case 29:out << "Smithy "; break;
	case 30:out << "None "; break;
	}
	out << district->getValue();
	return out;
}
