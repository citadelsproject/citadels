#pragma once
#include "Board.h"
#include "Player.h"
class BoardStateChecker
{
public:
	enum class State
	{
		None,
		Win,
		Draw
	};

public:
	static State Check(Player& firstplayer, Player& secondplayer);
};

