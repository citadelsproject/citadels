#include "CitadelsGame.h"

void CitadelsGame::Init()
{
	std::string playerName;
	std::cout << "First player's name:";
	std::cin >> playerName;
	firstPlayer.setName(playerName);

	std::cout << "Second player's name:";
	std::cin >> playerName;
	secondPlayer.setName(playerName);


	std::cout << " Which of you is the oldest ?\n";
	std::cin >> playerName;
	if (playerName == firstPlayer.getName())
	{
		firstPlayer.setKing(true);
		std::cout << "Now , " << firstPlayer.getName() << " is the King !\n";
	}
	else
	{
		secondPlayer.setKing(true);
		std::cout << "Now , " << secondPlayer.getName() << " is the King !\n";
	}
	m_deck.createDeck();
	m_characters.addAllCharacterCards();
	m_characters.printAllCharacterCards();

	CharacterCards *character = new CharacterCards();
	character = m_characters.pickCharacter();
	firstPlayer.m_character = character;

	character = m_characters.pickCharacter();
	secondPlayer.m_character = character;

	m_characters.setAllCharacterCardsVisible();
	DistrictCards *card = new DistrictCards();
	for (int index = 0; index < 4; ++index)
	{
		card = m_deck.pickCard();
		firstPlayer.addDistrictCard(card);
		card = m_deck.pickCard();
		secondPlayer.addDistrictCard(card);
	}
	while (firstPlayer.m_boardPlayer.nBoardCards() <= 7 || secondPlayer.m_boardPlayer.nBoardCards() <= 7)
	{
		playerTurn(firstPlayer, secondPlayer);
		playerTurn(secondPlayer, firstPlayer);
		m_characters.m_characterCards.clear();
		m_characters.addAllCharacterCards();
		m_characters.setAllCharacterCardsVisible();
		character = m_characters.pickCharacter();
		firstPlayer.m_character = character;
		character = m_characters.pickCharacter();
		secondPlayer.m_character = character;
		m_characters.setAllCharacterCardsVisible();

	}
	if (BoardStateChecker::Check(firstPlayer, secondPlayer) == BoardStateChecker::State::Win)
		std::cout << firstPlayer.getName() << " wins!!\n";
	else if (BoardStateChecker::Check(firstPlayer, secondPlayer) == BoardStateChecker::State::Draw)
		std::cout << "It's a draw!!!!!!\n";
	else
		std::cout << secondPlayer.getName() << " wins!!\n";
}

void CitadelsGame::playerTurn(Player &firstplayer, Player &secondplayer)
{
	int choose;
	int choose2;
	int ok = 0;
	k_nrOfRounds++;
	aiGetBestMove(aiAssistant(firstplayer, secondplayer));
	for (auto it = m_characters.m_characterCards.begin(); it != m_characters.m_characterCards.end(); it++)
	{
		if (firstPlayer.m_character == it->first)
			ok = 1;
	}
	if (ok == 0)
	{
		std::cout << "You have been killed by the assassin!!!";
		return;
	}
	std::cout << std::endl;
	std::cout << firstplayer.getName() << " handDeck is: \n";
	firstplayer.displayAllCards();
	std::cout << "You have " << firstplayer.getGold() << " gold\n";
	std::cout << "1.Take 2 cards.\n";
	std::cout << "2.Take 2 gold.\n";
	std::cin >> choose;
	switch (choose)
	{
	case 1: {
		DistrictCards *card1 = new DistrictCards();
		DistrictCards *card2 = new DistrictCards();
		card1 = m_deck.pickCard();
		card2 = m_deck.pickCard();
		std::cout << "Choose on of these two cards:\n";
		std::cout << "1. " << card1 << std::endl;
		std::cout << "2. " << card2 << std::endl;

		std::cin >> choose2;
		if (choose2 == 1)
			firstplayer.addDistrictCard(card1);
		else
			firstplayer.addDistrictCard(card2);
	}; break;

	case 2: {
		firstplayer.addGold(2);
	}; break;

	}
	std::cout << "Now, use your character's ability.\n";
	std::cout << "Your character ability is: \n";
	std::cout << firstplayer.m_character->getAbility();
	std::cout << std::endl;
	std::cout << "Use your character ability? (yes/no)";
	std::string answer;
	std::cin >> answer;
	if (answer == "yes")
	{
		switch (static_cast<int>(firstplayer.m_character->getCharacter()))
		{
			std::cout << std::endl;
		case 0: {
			int index;
			m_characters.printAllCharacterCards();
			std::cout << "Choose the index of the character you wish to kill:\n";
			std::cin >> index;
			auto it = m_characters.m_characterCards.begin();
			for (it = m_characters.m_characterCards.begin(); *it != m_characters.m_characterCards[index]; ++it);
			m_characters.m_characterCards.erase(it);
			buildDistricts(firstplayer);
		} break;
		case 1: {
			int index;
			m_characters.printAllCharacterCards();
			std::cout << "Choose the index of the character you wish to rob:\n";
			std::cin >> index;
			if (static_cast<CharacterCards::Characters>(index) == secondplayer.m_character->getCharacter())
			{
				firstplayer.addGold(secondplayer.getGold());
				secondplayer.setGold(0);
			}
			buildDistricts(firstplayer);
		} break;
		case 2: {
			std::cout << "Your handCards will be replaced with the other player's handCards\n";
			Hand aux = firstplayer.getHand();
			firstplayer.setHand(secondplayer.getHand());
			secondplayer.setHand(aux);
			buildDistricts(firstplayer);
		} break;
		case 3: {
			std::cout << "Now , you are the King !\n";
			firstplayer.setKing(true);
			buildDistricts(firstplayer);

		}break;
		case 4: {
			std::cout << "Warlord cannot destroy your districts !\n";
			buildDistricts(firstplayer);
		}break;

		case 5: {
			std::cout << "You gain one extra gold !\n";
			firstplayer.addGold(1);
			buildDistricts(firstplayer);
		}break;
		case 6: {
			std::cout << "Pick two extra cards !\n";
			DistrictCards *card = new DistrictCards();
			card = m_deck.pickCard();
			firstplayer.addDistrictCard(card);
			card = m_deck.pickCard();
			firstplayer.addDistrictCard(card);
			buildDistricts(firstplayer);
			buildDistricts(firstplayer);
			buildDistricts(firstplayer);

		}break;
		case 7: {
			if (secondplayer.m_character->getCharacter() == CharacterCards::Characters::Bishop)
			{
				std::cout << "You cannot destroy the districts of the bishop! ";
				return;
			}
			std::cout << "The other player's cards:\n";
			secondplayer.m_boardPlayer.DrawBoard();
			std::cout << "Choose the position of the district you wish to destroy .\n";
			int position;
			std::cin >> position;
			int cardGold = firstplayer.m_boardPlayer.DestroyDistrict(position)->getValue() - 1;
			firstplayer.setGold(firstplayer.getGold() - cardGold);
			buildDistricts(firstplayer);
		}break;
		}
	}
	else
		buildDistricts(firstplayer);
}
void CitadelsGame::buildDistricts(Player &player)
{
	Hand auxHand = player.getHand();
	std::cout << "Now , you can build one district .\n";
	std::cout << "Do you want to build ? (yes/no)\n";
	std::string answer;
	std::cin >> answer;
	if (answer == "yes")
	{
		int position = 0;
		player.displayAllCards();
		std::cout << "You have: " << player.getGold() << " gold.\n";
		while (player.canBuild(player.getGold()) == true)
		{
			std::cout << "Choose the position of the card you want to build .\n";
			std::cin >> position;
			if (player.getValueOfCard(position) > player.getGold())
				std::cout << "Choose another card.\n";
			else
			{
				break;
			}
		}
		DistrictCards *aux = new DistrictCards();
		aux = player.dropDistrictCard(position);
		int cardGold = aux->getValue();

		std::cout << "Your board is:\n";
		player.m_boardPlayer.DrawBoard();
		std::cout << "Choose the position where you want to place your card.\n";
		std::cin >> position;
		player.m_boardPlayer.PlaceDistrict(aux, position);
		player.setGold(player.getGold() - cardGold);
		player.m_boardPlayer.DrawBoard();

	}
	else
		return;
}

//AI assistant implementation based on MINIMAX algorithm 
std::vector<int> CitadelsGame::aiAssistant(Player &firstplayer, Player &secondplayer)
{
	std::vector<int>hints;
	hints.clear();

	if (firstplayer.getGold() <= 2)
		hints.push_back(15);
	else
		if (firstplayer.nrOfHandCards() <= 2)
			hints.push_back(10);

	if (firstplayer.nrOfRedDistricts() >= 2)
		hints.push_back(25);
	if (firstplayer.nrOfBlueDistricts() >= 2)
		hints.push_back(26);
	if (firstplayer.nrOfYellowDistricts() >= 2)
		hints.push_back(27);
	if (firstplayer.nrOfGreenDistricts() >= 2)
		hints.push_back(28);

	if (secondplayer.m_boardPlayer.nBoardCards() - firstplayer.m_boardPlayer.nBoardCards() >= 3)
		hints.push_back(-30);

	if (firstplayer.m_boardPlayer.nBoardCards() == 6)
		hints.push_back(30);

	if (secondplayer.getGold() - firstplayer.getGold() >= 4)
		hints.push_back(-15);
	else
		if (secondplayer.nrOfHandCards() - firstplayer.nrOfHandCards() >= 3)
			hints.push_back(-10);

	if (k_nrOfRounds <= 2)
		hints.push_back(5);
	return hints;
}

void CitadelsGame::aiGetBestMove(std::vector<int> vector)
{
	int minScore = 100;
	int maxScore = -100;
	for (int index = 0; index < vector.size(); ++index)
	{
		if (vector[index] < 100)
			minScore = vector[index];
		if (vector[index] > -100)
			maxScore = vector[index];
	}

	switch (minScore)
	{
	case -10:
	{
		std::cout << "HINT: Your opponent has more cards than you!\n You should choose Magician next round, so you can swap with his hand!";
	}break;
	case -15:
	{
		std::cout << "HINT: You should choose Thief next round, so you can steal your opponent's gold!";
	}break;
	case -30:
	{
		std::cout << "HINT: You should choose Warlord next round, so you can destroy some of your opponent's districts!";
	}break;
	}

	switch (maxScore)
	{
	case 5:
	{
		std::cout << "HINT: You should take some gold this round!";
	}break;
	case 10:
	{
		std::cout << "HINT: You are low on cards. You should draw some cards this round!";
	}break;
	case 15:
	{
		std::cout << "HINT: You are low on gold. You should take some gold this round or choose Thief, so you can steal your opponent's gold!";
	}break;
	case 25:
	{
		std::cout << "HINT: You should choose Warlord next round so you can gain gold for your RED districts!!";
	}break;
	case 26:
	{
		std::cout << "HINT: You should choose Bishop next round so you can gain gold for your BLUE districts!!";
	}break;
	case 27:
	{
		std::cout << "HINT: You should choose King next round so you can gain gold for your YELLOW districts!!";
	}break;
	case 28:
	{
		std::cout << "HINT: You should choose Merchant next round so you can gain gold for your GREEN districts!!";
	}break;
	case 30:
	{
		std::cout << "HINT: You should choose Bishop this round to protect your districts, \n or Architect to build more than 1 district if your opponent picks Warlord, so you can WIN!!";
	}break;
	}
}



