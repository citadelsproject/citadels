#pragma once
#include "CharacterCards.h"
#include "DistrictCards.h"
#include "Hand.h"
#include "Board.h"

class Player
{
public: Board m_boardPlayer;
		CharacterCards* m_character;
		bool m_king;
public:
	Player();
	Player(const std::string& name);
	~Player();

	const void addGold(int gold);
	const int getGold() const;
	const std::string getName() const;
	const void setName(std::string &name);
	const void setGold(int gold);
	const Hand getHand() const;
	const void setHand(Hand hand);
	const void setKing(bool king);
	const bool getKing() const;
	int getValueOfCard(int position);
	void addDistrictCard(DistrictCards* &card);
	DistrictCards* dropDistrictCard(int position);
	void displayAllCards();
	bool canBuild(int gold);
	int nrOfHandCards();
	int nrOfRedDistricts();
	int nrOfBlueDistricts();
	int nrOfYellowDistricts();
	int nrOfGreenDistricts();
	
private:
	std::string m_name;
	int m_gold;
	Hand m_handCards;
};

