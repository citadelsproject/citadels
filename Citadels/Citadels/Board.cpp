#include "Board.h"


const void Board::DrawBoard()
{
	int index = 0;
	for (auto it=m_cards.begin();it!=m_cards.end();++it,index++)
	{
		if (*it != std::nullopt)
			std::cout << m_cards[index].value() << std::endl;
	}
	std::cout << std::endl;
}

void Board::PlaceDistrict(DistrictCards* &dc,int position)
{
	m_cards[position].emplace(dc);
}

DistrictCards* Board::DestroyDistrict(int position)
{
	DistrictCards* aux = new DistrictCards();
	aux = m_cards[position].value();
	m_cards[position].emplace(nullptr);
	return aux;
}

int Board::nBoardCards()
{
	int k = 0;
	for (auto it = m_cards.begin(); it != m_cards.end(); ++it)
		if (*it != std::nullopt)
			k++;
	return k;
}

int Board::getAllGold()
{
	int sum = 0;
	for (auto it = m_cards.begin(); it != m_cards.end(); ++it)
		sum += it->value()->getValue();
	return sum;
}


std::optional<DistrictCards*>& Board::operator[](const int &position)
{
	return m_cards[position];
}
