#include "Hand.h"

void Hand::displayHandCards()
{
	for (auto it = m_hand.begin(); it != m_hand.end(); ++it)
	{
		std::cout << *it << std::endl;
	}
	std::cout << std::endl;
	std::cout << std::endl;
}

void Hand::addHandCards(DistrictCards* &card)
{
	m_hand.push_back(card);
}

DistrictCards* Hand::dropHandCards(int position)
{
	DistrictCards *aux = new DistrictCards();
	auto it = m_hand.begin();
	for (; position != 0; ++it, --position);
	aux = *it;
	m_hand.erase(it);
	return aux;
}

int Hand::getCard(int position)
{
	return m_hand[position]->getValue();
}

bool Hand::canBuild(int gold)
{
	for (int index = 0; index < m_hand.size(); ++index)
	{
		if (m_hand[index]->getValue() <= gold)
			return true;
	}
	return false;
}

int Hand::nrOfCards()
{
	int sum = 0;
	for (auto it = m_hand.begin(); it < m_hand.end(); ++it)
	{
		sum++;
	}
	return sum;
}

