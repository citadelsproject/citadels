#pragma once
#include"DistrictCards.h"

class Hand
{
public:
	Hand()=default;
	~Hand()=default;
	void displayHandCards();
	void addHandCards(DistrictCards* &card);
	DistrictCards* dropHandCards(int position);
	int getCard(int position);
	bool canBuild(int gold);
	int nrOfCards();
private:
	std::vector<DistrictCards*>m_hand;
};

