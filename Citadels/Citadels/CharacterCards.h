#pragma once
#include<string>
#include<vector>
#include<unordered_map>


class CharacterCards
{
public:
	enum class Characters
	{
		Assassin,
		Thief,
		Magician,
		King,
		Bishop,
		Merchant,
		Architect,
		Warlord,
		None
	};
public: std::vector<std::pair<CharacterCards*,bool>> m_characterCards;
public:
	CharacterCards();
	CharacterCards(Characters characters);
	CharacterCards(CharacterCards && other);
	CharacterCards &operator=(CharacterCards &&other);
	CharacterCards &operator=(CharacterCards* &other);
	~CharacterCards();

	const Characters getCharacter() const;
	const void setCharacters(Characters character);
	const std::string& getAbility() const;
	const void setAbility(std::string& ability);

	friend std::ostream& operator<<(std::ostream&,CharacterCards* &ch);

	void addAllCharacterCards();
	void printAllCharacterCards();
	void setAllCharacterCardsVisible();
	CharacterCards* pickCharacter();

private:
	Characters m_character;
	std::string m_ability;
};

