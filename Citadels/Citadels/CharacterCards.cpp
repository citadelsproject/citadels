#include "CharacterCards.h"

#include<iostream>
#include<fstream>
#include<iterator>
#include<random>

CharacterCards::CharacterCards():
	m_character(CharacterCards::Characters::None),
	m_ability("unknown")
{
}

CharacterCards::CharacterCards(Characters characters):
m_character(characters)
{
}

CharacterCards::CharacterCards(CharacterCards && other)
{
	*this = std::move(other);
}

CharacterCards & CharacterCards::operator=(CharacterCards &&other)
{
	new(&other) CharacterCards;
	return *this;
}

CharacterCards & CharacterCards::operator=(CharacterCards* & other)
{
	m_character = other->m_character;
	m_ability = other->m_ability;
	return *this;
}

CharacterCards::~CharacterCards()
{
	m_character = Characters::None;
}
const CharacterCards::Characters CharacterCards::getCharacter() const
{
	return m_character;
}
const void CharacterCards::setCharacters(Characters character)
{
	m_character = character;
}

const std::string& CharacterCards::getAbility() const
{
	return m_ability;
}
const void CharacterCards::setAbility(std::string & ability)
{
	m_ability = ability;
}
void CharacterCards::addAllCharacterCards()
{
	std::ifstream fin("addAllCharacters.txt");
	std::string ability;
	for (int index = 0; index < 8 && std::getline(fin,ability); ++index)
	{
		CharacterCards *character = new CharacterCards();
		character->setCharacters(static_cast<Characters>(index));
		character->setAbility(ability);
		m_characterCards.push_back(std::make_pair(character,true));
	}
}

void CharacterCards::printAllCharacterCards()
{
	int index = 0;
	for (auto it = m_characterCards.begin(); it != m_characterCards.end(); ++it,index++)
	{
		if (it->second == true)
			std::cout << index << ". " << it->first << std::endl;
	}
	std::cout << std::endl;
}

void CharacterCards::setAllCharacterCardsVisible()
{
	for (auto it = m_characterCards.begin(); it != m_characterCards.end(); ++it)
		it->second = true;
}

CharacterCards* CharacterCards::pickCharacter()
{
	std::cout << "Choose the id of your Character:\n";
	int index;
	std::cin >> index;
	if (index < 0 || index>7)
		throw "Character invalid";

	auto it = m_characterCards.begin();
	for (; index != 0; it++, index--);
	CharacterCards *character = it->first;
	it->second = false;
	
	return character;
}
std::ostream& operator<<(std::ostream& out, CharacterCards* &ch)
{
			switch (static_cast<int>(ch->m_character))
			{
			case 0:out << "Assasin"; break;
			case 1:out << "Thief"; break;
			case 2:out << "Magician"; break;
			case 3:out << "King"; break;
			case 4:out << "Bishop"; break;
			case 5:out << "Merchant"; break;
			case 6:out << "Arhitect"; break;
			case 7:out << "Warlord"; break;
			case 8:out << "None"; break;
			default:
				break;
			}
			out << " has ability: " << ch->m_ability;
	return out;
}

	
